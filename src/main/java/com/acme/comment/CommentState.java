package com.acme.comment;

public interface CommentState {
	
	void approve();
	
	void reject();
	
	void remove();
}
