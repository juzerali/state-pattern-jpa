package com.acme.comment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Comment implements CommentState {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String text = "";
	
	@Column 
	private String remark = "";
	
	@Column(columnDefinition="ENUM('PENDING','APPROVED','REJECTED','REMOVED')")
	@Enumerated(EnumType.STRING)
	private State state;
	
	/**
	 * Constructor for hibernate to instantiate this class after fetching from database
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private Comment() {}
	
	public Comment(String text) {
		setText(text);
		setState(State.PENDING);
	}
	
	public void approve() {
		state.approve();
	}
	
	public void reject() {
		state.reject();
	}
	
	public void remove() {
		state.remove();
	}

	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	/**
	 * Comment text is immutable. Private setter for Hibernate to hydrate the object.
	 * 
	 * @param text
	 */
	private void setText(String text) {
		this.text = text;
	}

	public State getState() {
		return state;
	}
	
	/**
	 * Visibility is package-private, this prevents clients from changing state directly, or if the client is within the project, developer 
	 * will have to explain why his class resides in comment package but allows Status class (in the same package) to change the state.
	 * 
	 * @param status
	 */
	void setState(State status) {
		this.state = status;
		status.setComment(this);
	}
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
