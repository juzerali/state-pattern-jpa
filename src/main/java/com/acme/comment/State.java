package com.acme.comment;


public enum State implements CommentState {
	
	/**
	 * =============================================================================
	 * A PENDING comment can either be REJECTED or ACCEPTED
	 * =============================================================================
	 */
	PENDING {
		
		public void approve() {
			comment.setState(APPROVED);
		}
		
		public void reject() {
			String remark = comment.getRemark();
			if(remark == null || remark.isEmpty()) {
				throw new IllegalStateException("Can't reject without remark");
			}
			
			comment.setState(REJECTED);
		}
	},
	
	
	/**
	 * =============================================================================
	 *  Once APPROVED, comment can only be removed
	 * =============================================================================
	 */
	APPROVED {
		public void remove() {
			comment.setState(REMOVED);
		}
	},
	
	
	/**
	 * =============================================================================
	 * Once REJECTED or REMOVED no more transitions possible
	 * =============================================================================
	 */
	REJECTED,
	REMOVED;
	
	
	/* 
	 * ------------------------------------------------------------------------------- 
	 * Defaults 
	 * ------------------------------------------------------------------------------- 
	 */
	
	public void approve() {
		throwIt();
	}
	
	public void reject() {
		throwIt();
	}
	
	public void remove() {
		throwIt();
	}
	
	private void throwIt() {
		throw new IllegalStateException();
	}
	
	@JsonIgnore
	protected Comment comment;
	
	void setComment(Comment comment) {
		this.comment = comment;
	}
	
}
