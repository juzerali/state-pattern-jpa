package com.acme;

import org.springframework.data.repository.CrudRepository;

import com.acme.comment.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {

}
